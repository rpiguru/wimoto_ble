from bluepy.bluepy.btle import *
import os
from bluepy.bluepy.btle import _UUIDNameMap
import xml.etree.ElementTree

ADDR_TYPE_PUBLIC = "public"
ADDR_TYPE_RANDOM = "random"

conf_file_name = "config.xml"

class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            pass
        elif isNewData:
            print "Received new data from", dev.addr


def _TI_UUID(val):
    return UUID("%08X-0451-4000-b000-000000000000" % (0xF0000000+val))


def get_json_uuid():
    import json
    with open(os.path.join(script_path, 'uuids.json'),"rb") as fp:
        uuid_data = json.loads(fp.read().decode("utf-8"))
    for k in ['service_UUIDs', 'characteristic_UUIDs', 'descriptor_UUIDs' ]:
        for number,cname,name in uuid_data[k]:
            yield UUID(number, cname)
            yield UUID(number, name)


def get_param_from_xml(param):
    """
    Get configuration parameters from the config.xml
    :param param: parameter name
    :return: if not exists, return None
    """
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
            break
    return tmp

if __name__ == "__main__":
    if os.geteuid() != 0:
        exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    AssignedNumbers = _UUIDNameMap(list(get_json_uuid()))

    scanner = Scanner().withDelegate(ScanDelegate())
    devices = scanner.scan(4.0)

    mac_list = []
    for dev in devices:
        print "Device %s (%s), RSSI=%d dB" % (dev.addr, dev.addrType, dev.rssi)
        for (adtype, desc, value) in dev.getScanData():
            print " TYPE: %s,    %s = %s" % (adtype, desc, value)
            if desc == 'Complete Local Name':
                if value[:6] == 'Sentry':
                    mac_list.append((dev.addr, value))

    print "Wimoto list : ", mac_list

    print "Attempting to connect..."


    for mac in mac_list:

        devAddr = mac[0]
        addrType = ADDR_TYPE_RANDOM
        print("Connecting to: {}, address type: {}".format(devAddr, addrType))
        print "Device name is ", mac[1]

        conn = Peripheral(devAddr, addrType)
        try:
            uuid = get_param_from_xml("UUID")
            svc = conn.getServiceByUUID(uuid)

            print(str(svc), ":")

            for ch in svc.getCharacteristics():
                print("    {}, hnd={}, supports {}".format(ch, hex(ch.handle), ch.propertiesToString()))
                chName = AssignedNumbers.getCommonName(ch.uuid)
                if (ch.supportsRead()):
                    try:
                        print("Handle : %s, value : %s " % (hex(ch.handle), repr(ch.read())))
                    except BTLEException as e:
                        print("    ->", e)
                print ""


        finally:
            conn.disconnect()


