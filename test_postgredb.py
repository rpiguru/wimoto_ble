import xml.etree.ElementTree

import datetime
import psycopg2
import time

conf_file_name = "config.xml"

debug = 1


def get_param_from_xml(param):
    """
    Get configuration parameters from the config.xml
    :param param: parameter name
    :return: if not exists, return None
    """
    root = xml.etree.ElementTree.parse(conf_file_name).getroot()
    tmp = None
    for child_of_root in root:
        if child_of_root.tag == param:
            tmp = child_of_root.text
            break
    return tmp


def upload_db(my_dev):
    print "Uploading data."
    print "Device name : %s, value : %s, Strength : %s" % my_dev

    host_name = get_param_from_xml('HOST_NAME')
    db_name = get_param_from_xml('DB_NAME')
    username = get_param_from_xml('USER_NAME')
    password = get_param_from_xml('PASSWORD')

    conn = None
    try:
        uri = "host=" + host_name + " dbname=" + db_name + " user=" + username + " password=" + password
        conn = psycopg2.connect(uri)
    except:
        print "Failed to connect to database"
        return False

    cur = conn.cursor()
    try:
        query = "CREATE TABLE IF NOT EXISTS securimote (datetime varchar(30) NOT NULL, " \
            "device_name varchar(30) NOT NULL, rssi integer NOT NULL, PRIMARY KEY (datetime));"
        cur.execute(query)
        conn.commit()

        str_time = datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

        query = "INSERT INTO securimote (datetime, device_name, rssi) VALUES " \
                "('" + str_time + "', '" + my_dev[0] + "', " + my_dev[2] + ")"
        cur.execute(query)
        conn.commit()

    except psycopg2.DatabaseError, e:
        print "Error"
        print e
        if conn:
            conn.rollback()
    finally:
        if conn:
            conn.rollback()


upload_db(('Sentry_155676', 'mac', '-25'))
upload_db(('Sentry_22bcb6', 'mac', '-25'))
