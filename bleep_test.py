from bleep import BLEDevice
import sys

index = sys.argv[1]

devices = BLEDevice.discoverDevices()

print "Device List : ", devices

device = devices[int(index)]

print "Device : ", device, ", connecting..."

device.connect()

print "Services : ", device.services

service = device.services.data

print "Service 0 : ", service

char = service.characteristics[0]

print "Character : ", char

