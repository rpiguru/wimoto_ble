import os

import signal


def kill_bluepy_proc():
    """
    Kill bluepy-helper process to restart thread again
    :param i: number of process
    :return:
    """
    pid_list = []

    cmd_result = os.popen("ps ax | grep bluepy-helper | grep -v grep")

    for line in cmd_result:
        fields = line.split()
        print "Field: ", fields
        pid_list.append(fields[0])

    print pid_list

    # if app is already running, the list's size is greater than 2
    if pid_list:
        for pid in pid_list:
            os.kill(int(pid), signal.SIGKILL)	# kill 1st process(old)
        return True
    else:
        return False

if __name__ == "__main__":
    kill_bluepy_proc()

    # address = '10:40:F3:E5:EB:8C'
    #
    # for device in BLEDevice.discoverDevices():
    #     if str(device.address) != address:
    #         continue
    #
    #     try:
    #         print("Attempting to connect to %s" % device.address)
    #         device.connect()
    #
    #         for service in device.services:
    #             print("  " + repr(service))
    #
    #             for characteristic in service.characteristics:
    #                 print("Character : " + repr(characteristic))
    #
    #                 for descriptor in characteristic.descriptors:
    #                     print("Descriptor : " + repr(descriptor))
    #
    #         break
    #     except:
    #         device.disconnect()
    #         raise
    #     finally:
    #         device.disconnect()
    # else:
    #     # break didn't get called
    #     print('Device not Found')