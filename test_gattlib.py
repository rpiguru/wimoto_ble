#!/usr/bin/python
# -*- mode: python; coding: utf-8 -*-

# Copyright (C) 2014, Oscar Acena <oscaracena@gmail.com>
# This software is under the terms of Apache License v2 or later.

from __future__ import print_function

import sys, time
from gattlib import GATTRequester, GATTResponse, DiscoveryService
from threading import Event


class Requester(GATTRequester):
    def __init__(self, wakeup, *args):
        GATTRequester.__init__(self, *args)
        self.wakeup = wakeup

    def on_notification(self, handle, data):
        print("- notification on handle: {}\n".format(handle))
        self.wakeup.set()


class ReceiveNotification(object):
    def __init__(self, address):
        self.received = Event()
        self.requester = Requester(self.received, address, False)

        self.connect()
        self.wait_notification()

    def connect(self):
        print("Connecting...", end=' ')
        sys.stdout.flush()

        self.requester.connect(True)
        print("OK!")

    def wait_notification(self):
        print("\nThis is a bit tricky. You need to make your device to send\n"
              "some notification. I'll wait...")
        self.received.wait()


class Reader(object):
    def __init__(self, address):
        self.requester = GATTRequester(address, False)
        self.connect()
        self.request_data()

    def connect(self):
        print("Connecting...", end=' ')
        sys.stdout.flush()

        self.requester.connect(True)
        print("OK!")

    def request_data(self):
        data = self.requester.read_by_handle(0x1)[0]

        print("bytes received:", end=' ')
        for b in data:
            print(hex(ord(b)), end=' ')
        print("")


class AsyncReader(object):
    def __init__(self, address):
        self.requester = GATTRequester(address, False)
        self.response = GATTResponse()

        self.connect()
        self.request_data()
        self.wait_response()

    def connect(self):
        print("Connecting...", end=' ')
        sys.stdout.flush()

        self.requester.connect(True)
        print("OK!")

    def request_data(self):
        self.requester.read_by_handle_async(0x1, self.response)

    def wait_response(self):
        while not self.response.received():
            time.sleep(0.1)

        data = self.response.received()[0]

        print("bytes received:", end=' ')
        for b in data:
            print(hex(ord(b)), end=' ')
        print("")

if __name__ == '__main__':

    print("Discovering...")
    service = DiscoveryService("hci0")
    devices = service.discover(4)

    add_list = []
    for address, name in list(devices.items()):
        add_list.append(address)
        print("name: {}, address: {}".format(name, address))

    print("Discovering Done.")
    add_list.reverse()
    # print("Receiving notification...")
    # for mac in add_list:
    #     ReceiveNotification(mac)
    #
    #
    # print("Discovering Primary..")
    # for mac in add_list:
    #     requester = GATTRequester(mac, False)
    #
    #     print("Connecting to %s..." % mac)
    #     sys.stdout.flush()
    #     requester.connect(True)
    #
    #     primary = requester.discover_primary()
    #     for prim in primary:
    #         print(prim)

    print("Reading...")
    for mac in add_list:
        print(mac)

        Reader(mac)

        print("Reading Done.")

        time.sleep(1)
        print("Async Reading")
        AsyncReader(mac)

        print("Async reading done")
